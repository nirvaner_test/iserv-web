angular.module('app').controller('visitCtrl', visitCtrl);

function visitCtrl($scope, $stateParams, dataSvc, HostApi, $state) {
    var vm = $scope;

    var id = $stateParams.id;
    vm.model = {VisitDate: new Date()};
    vm.valErrs = {};
    vm.wasValidated = false;

    vm.visitorOpt = {
        dataTextField: "Name",
        dataValueField: "Id",
        valuePrimitive: true,
        filter: 'contains',
        minLength: 3,
        dataSource: {
            serverFiltering: true,
            transport: {
                read: {
                    dataType: 'json',
                    url: HostApi + "/api/visitors/search"
                },
                parameterMap: function (data, action) {
                    if (data.filter && data.filter.filters) {
                        return {term: data.filter.filters[0].value, page: 1, limit: 10};
                    } else {
                        return {page: 1, limit: 10};
                    }
                }
            }
        }
    };

    vm.save = function () {
        if (!vm.model.VisitorId) vm.valErrs.VisitorId = true;
        if (!vm.model.VisitDate) vm.valErrs.VisitDate = true;
        if (vm.valErrs.VisitDate || vm.valErrs.VisitorId) {
            vm.wasValidated = true;
            vm.valErrs = {};
            return;
        }
        vm.wasValidated = false;
        dataSvc.post(HostApi,
            '/api/visits',
            vm.model)
            .then(function (res) {
                if (res.status !== 200) console.error(res.data);
                if (res.status === 200) return $state.go('app.visits');
            });
    };
}