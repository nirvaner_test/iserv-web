angular.module('app').controller('visitsCtrl', visitsCtrl);

function visitsCtrl($scope, $state, dataSvc, HostApi) {
    var vm = $scope;
    $scope.visitsCtrl = vm;

    vm.perPage = '10';
    vm.page = 1;
    vm.pageCount = 1;
    vm.filter = {
        status: false,
        term: ''
    };
    vm.pages = function (number) {
        return new Array(number);
    };
    vm.perPageChange = function (e) {
        refresh();
    };
    vm.changePage = function (e, page) {
        vm.page = page;
        refresh();
    };
    vm.search = function (e, del) {
        if (del) {
            vm.filter.term = '';
        }
        vm.page = 1;
        refresh();
    };
    vm.toDetails = function (id) {
        $state.go('app.visitorDetails', {id: id});
    };

    vm.list = [];

    function refresh() {
        var params = {
            page: vm.page,
            limit: vm.perPage,
            term: vm.filter.term
        };
        dataSvc.get(HostApi, '/api/visits/paged', params)
            .then(function (res) {
                if (res.status !== 200) return;
                var totalCount = res.data.count;
                vm.pageCount = Math.ceil(totalCount / vm.perPage);
                if (vm.page > vm.pageCount) vm.page = vm.pageCount;
                vm.list = res.data.items;
            })
    }

    refresh();
}