angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', '$httpProvider',
        function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider, $httpProvider) {

            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            $urlRouterProvider.otherwise('/visitors');

            $ocLazyLoadProvider.config({
                debug: false
            });

            $breadcrumbProvider.setOptions({
                prefixStateName: 'app',
                includeAbstract: true,
                template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
            });

            $stateProvider
                .state('app', {
                    abstract: true,
                    templateUrl: 'views/common/layouts/full.html',
                    //page title goes here
                    ncyBreadcrumb: {
                        label: 'Root',
                        skip: true
                    },
                    resolve: {
                        loadCSS: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load CSS files
                            return $ocLazyLoad.load([{
                                serie: true,
                                name: 'Font Awesome',
                                files: ['css/font-awesome.min.css']
                            }, {
                                serie: true,
                                name: 'Simple Line Icons',
                                files: ['css/simple-line-icons.css']
                            }]);
                        }],
                        loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load files for an existing module
                            return $ocLazyLoad.load([{
                                serie: true,
                                name: 'chart.js',
                                files: [
                                    'bower_components/chart.js/dist/Chart.min.js',
                                    'bower_components/angular-chart.js/dist/angular-chart.min.js'
                                ]
                            }]);
                        }]
                    }
                })
                .state('app.visitors', {
                    url: '/visitors',
                    templateUrl: 'app/visitors/visitors.html',
                    ncyBreadcrumb: {
                        label: 'Посетители'
                    },
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                files: ['app/visitors/visitorsCtrl.js']
                            });
                        }]
                    }
                })
                .state('app.visitorDetails', {
                    url: '/visitors/:id',
                    templateUrl: 'app/visitors/visitor.html',
                    ncyBreadcrumb: {
                        parent: 'app.visitors',
                        label: 'Посетитель'
                    },
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: ['app/visitors/visitorCtrl.js']
                            });
                        }]
                    }
                })
                .state('app.visits', {
                    url: '/visits',
                    templateUrl: 'app/visits/visits.html',
                    ncyBreadcrumb: {
                        label: 'Посещения'
                    },
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                files: ['app/visits/visitsCtrl.js']
                            });
                        }]
                    }
                })
                .state('app.visitDetails', {
                    url: '/visits/:id',
                    templateUrl: 'app/visits/visit.html',
                    ncyBreadcrumb: {
                        parent: 'app.visits',
                        label: 'Посещение'
                    },
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: ['app/visits/visitCtrl.js']
                            });
                        }]
                    }
                })
                .state('app.references', {
                    url: '/references',
                    abstract: true,
                    template: '<ui-view></ui-view>',
                    ncyBreadcrumb: {
                        label: 'Справочники'
                    }
                })
                .state('app.references.types', {
                    url: '/types',
                    templateUrl: 'app/types/types.html',
                    ncyBreadcrumb: {
                        label: 'Типы ПЛ'
                    },
                    resolve: {
                        loadMyCrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                files: ['app/types/typesCtrl.js']
                            })
                        }]
                    }
                })
                .state('app.references.typeDetails', {
                    url: '/types/:id',
                    templateUrl: 'app/types/type.html',
                    ncyBreadcrumb: {
                        parent: 'app.references.types',
                        label: '.'
                    },
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                files: ['app/types/typeCtrl.js']
                            })
                        }]
                    }
                })
        }]);