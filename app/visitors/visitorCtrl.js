angular.module('app').controller('visitorCtrl', visitorCtrl);

function visitorCtrl($scope, $stateParams, dataSvc, HostApi, $state) {
    var vm = $scope;

    var id = $stateParams.id;
    vm.model = {};
    vm.valErrs = {};
    vm.wasValidated = false;

    vm.save = function () {
        if (!vm.model.Name) vm.valErrs.Name = true;
        if (!vm.model.Surname) vm.valErrs.Surname = true;
        if (!vm.model.Birthdate) vm.valErrs.Birthdate = true;
        if (vm.valErrs.Name || vm.valErrs.Surname || vm.valErrs.Birthdate) {
            vm.wasValidated = true;
            vm.valErrs = {};
            return;
        }
        vm.wasValidated = false;
        var method = id === 'new' ? 'post' : 'put';
        dataSvc[method](HostApi,
            '/api/visitors' + (id === 'new' ? '' : '/' + id),
            vm.model)
            .then(function (res) {
                if (res.status !== 200) console.error(res.data);
                if (res.status === 400 && res.data.Message === "Already exists")
                    alert("Посетитель с такими именем и фамилией уже есть в системе");
                if (res.status === 200) return $state.go('app.visitors');
            });
    };

    if (id === 'new') {
        vm.model = {
            Sex: 0,
            Birthdate: new Date()
        };
    }
    else
        dataSvc.get(HostApi, '/api/visitors/' + id)
            .then(function (res) {
                if (res.status !== 200) return;
                vm.model = res.data;
                vm.model.Sex += '';
            });
}