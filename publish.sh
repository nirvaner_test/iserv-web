#!/usr/bin/env bash
docker swarm init

docker network create \
--driver overlay \
--subnet 10.1.1.0/24 \
--opt encrypted \
inspection

docker service create \
--detach=false \
--with-registry-auth \
--replicas 1 \
--publish 80:80 \
--name iserv-web \
registry.gitlab.com/nirvaner_test/iserv-web:master